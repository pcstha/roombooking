﻿using System.Web;
using System.Web.Mvc;

namespace GodwariHotelRoomBooking
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
