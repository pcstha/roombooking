﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GodwariHotelRoomBooking.Startup))]
namespace GodwariHotelRoomBooking
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
